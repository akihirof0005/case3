package case3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {
    public static void main(String[] args) {
            //判定する文字列
    //String str = "123A5";
    String str = "01234456";

    //判定するパターンを生成
    Pattern p = Pattern.compile("^[0-9]*$");
    Matcher m = p.matcher(str);

    //画面表示
    System.out.println(m.find());
    }
}
